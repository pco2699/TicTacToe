import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;

public class App extends Application {
    private int player_num = 1;
    private Label player_label = new Label("Player 1");
    private ArrayList<TicTacToeButton> buttons;
    private Boolean isGameEnded = false;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
         buttons = new ArrayList<>();

        for(int i = 0; i < 9; i++){
            buttons.add(new TicTacToeButton());
        }


        VBox vBox = new VBox(20d);
        vBox.setAlignment(Pos.CENTER);
        vBox.setPadding(new Insets(10));


        vBox.getChildren().add(player_label);

        for(int i = 0; i < 3; i++){
            HBox hBox = new HBox(20d);
            hBox.setAlignment(Pos.CENTER);
            hBox.getChildren().add(buttons.get(3 * i));
            hBox.getChildren().add(buttons.get(3 * i + 1));
            hBox.getChildren().add(buttons.get(3 * i + 2));
            vBox.getChildren().add(hBox);
        }

        for(int i = 0; i < buttons.size(); i++){
            buttons.get(i).setBehavior();
        }

        Scene scene = new Scene(vBox, 240, 260);
        primaryStage.setScene(scene);
        primaryStage.show();


    }
    public void isFull() {
        if(isGameEnded) {
           return;
        }
        // フル判定
        int fullRes = 0;
        for (int i = 0; i < buttons.size(); i++) {
            if (!buttons.get(i).getText().equals("")) {
                fullRes++;
            }
        }
        if (fullRes == buttons.size()) {
            player_label.setText("Draw!");
            isGameEnded = true;
        }
    }

    private void judgeResult(){
        if(isGameEnded){
            return;
        }
        // 勝ち負け判定
        // 縦列を判定
        for(int i = 0;i < 3; i++){
            if(!buttons.get(3 * i).getText().equals("") && !buttons.get(3 * i + 1).getText().equals("") && !buttons.get(3 * i + 1).getText().equals("")) {
                if (buttons.get(3 * i).getText().equals(buttons.get(3 * i + 1).getText()) && buttons.get(3 * i).getText().equals(buttons.get(3 * i + 2).getText())) {
                    setPlayer_label(buttons.get(3 * i).getText());
                }
            }
        }

        // 横列を判定
        for(int i = 0;i < 3; i++){
            if(!buttons.get(i).getText().equals("") && !buttons.get(i + 3).getText().equals("") && !buttons.get(i + 6).getText().equals("")) {
                if (buttons.get(i).getText().equals(buttons.get(i + 3).getText()) && buttons.get(i).getText().equals(buttons.get(i + 6).getText())) {
                    setPlayer_label(buttons.get(i).getText());
                }
            }
        }

        // 斜めを判定
        if(!buttons.get(2).getText().equals("") && !buttons.get(4).getText().equals("") && !buttons.get(6).getText().equals("")) {
            if (buttons.get(2).getText().equals(buttons.get(4).getText()) && buttons.get(2).getText().equals(buttons.get(6).getText())) {
                setPlayer_label(buttons.get(2).getText());
            }
        }

        if(!buttons.get(0).getText().equals("") && !buttons.get(4).getText().equals("") && !buttons.get(8).getText().equals("")) {
            if (buttons.get(0).getText().equals(buttons.get(4).getText()) && buttons.get(0).getText().equals(buttons.get(8).getText())) {
                setPlayer_label(buttons.get(0).getText());
            }
        }

    }

    public void setPlayer_label(String label){
        if(label.equals("◯")){
            player_label.setText("Player 1 won!");
        }
        else {
            player_label.setText("Player 2 won!");
        }
        isGameEnded = true;

    }

    public class TicTacToeButton extends Button {
        Boolean isPressed;

        TicTacToeButton(){
            isPressed = false;
            this.setPrefSize(50, 50);
        }


        public void setBehavior(){
                this.setOnAction(event -> {
                    if(!isPressed && !isGameEnded) {
                        if (player_num == 1) {
                            this.setText("◯");

                            player_label.setText("Player 2");
                            player_num = 2;
                        } else {
                            this.setText("☓");

                            player_label.setText("Player 1");
                            player_num = 1;
                        }
                        isPressed = true;
                        judgeResult();
                        isFull();
                    }
                });
        }
    }
}
